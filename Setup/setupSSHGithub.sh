#!/bin/bash
#
#Adds an ssh key with no password for use with github, then prints out the public key

if [ ! -f ~/.ssh/id_rsa.pub ]; then
    ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa

    #This part is unneeded
    #ssh-add ~/.ssh/id_rsa

    cat ~/.ssh/id_rsa.pub
else
    echo -e '\nExisting SSH Keys Detected! Printing out the existing key now: \n\n'
    cat ~/.ssh/id_rsa.pub
fi

