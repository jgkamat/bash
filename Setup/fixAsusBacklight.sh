#!/bin/bash

#works on the asus x200 to fix backlight (use if not working by default)
# To fix other problems IN FEDORA remove 'nomodset' from grub paramters and grub-update

echo 'Section "Device"
        Identifier  "card0"
        Driver      "intel"
        Option      "Backlight"  "intel_backlight"
        BusID       "PCI:0:2:0"

EndSection' > /usr/share/X11/xorg.conf.d/20-intel.conf
