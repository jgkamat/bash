#!/bin/bash

# This manually installs firefox, if not installed already

which firefox
if [ "$?" -ne 1 -o "$1" == "-f" ]; then
    echo firefox already installed!
    exit 1
fi

# exit immediately if error occured
set -e

# downloads *firefox 33* from mozilla
wget -O /tmp/firefox.tar.bz2 'https://download.mozilla.org/?product=firefox-33.1-SSL&os=linux64&lang=en-US'

# create firefox directory if not existing
sudo tar -jxvf /tmp/firefox.tar.bz2 -C /opt
sudo chown -R root:$(whoami) /opt/firefox
sudo chmod 750 /opt/firefox
sudo chmod +x /opt/firefox/firefox

sudo ln -f -s /opt/firefox/firefox /usr/bin/firefox

sudo rm /tmp/firefox.tar.bz2

echo 'cat << EOF > /usr/share/applications/firefox.desktop
[Desktop Entry]
Encoding=UTF-8
Name=Mozilla Firefox
Comment=Browse the World Wide Web
Type=Application
Terminal=false
Exec=/usr/bin/firefox %U
Icon=/opt/firefox/browser/icons/mozicon128.png
StartupNotify=true
Categories=Network;WebBrowser;
EOF' | sudo bash

