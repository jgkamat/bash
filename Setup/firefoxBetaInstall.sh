#!/bin/bash

# This manually installs firefox, if not installed already

which firefox
if [ "$?" -ne 1 -o "$1" == "-f" ]; then
    echo firefox already installed!
    exit 1
fi

# exit immediately if error occured
set -e


echo 'deb http://ppa.launchpad.net/mozillateam/firefox-next/ubuntu trusty main > /etc/apt/sources.list.d/mozillateam-firefox-next-jessie.list' | sudo bash

sudo apt-get update
sudo apt-get install firefox
