#!/bin/bash

# Become root
if [ $UID -ne 0 ]; then
	echo "-- Becoming root"
	exec sudo $0
fi

if [ -z "`grep 'archlinuxfr' /etc/pacman.conf`" ]; then

    cat << EOF >> /etc/pacman.conf

[archlinuxfr]
SigLevel = Never
Server = http://repo.archlinux.fr/\$arch

EOF

pacman -Sy yaourt

else
    echo yaourt already installed!

fi

