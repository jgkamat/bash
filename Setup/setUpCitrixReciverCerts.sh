#!/bin/bash
#DEBIAN FIX:
# https://help.ubuntu.com/community/CitrixICAClientHowTo
# https://wiki.debian.org/CitrixReceiver

cd /opt/Citrix/ICAClient/keystore/cacerts/ 
wget http://www.tbs-x509.com/AddTrustExternalCARoot.crt
