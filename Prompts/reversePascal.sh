#!/bin/bash
# For Nick


FLOPS=0;

# Takes in a list of numbers, space seperated
reduceList() {
	LIST=($@)
	INDEX=1 # idk why we skip the first but whatever.

	while [ $INDEX -lt ${#LIST[*]} ]; do
		LIST[$INDEX]="$(expr ${LIST[$INDEX]} - ${LIST[$(expr $INDEX - 1)]})"
		INDEX=$(expr $INDEX + 1)
	done

	# Clear 0s at end
	LIST=($(echo ${LIST[*]} | sed 's/\( 0\)*$//g'))

	echo ${LIST[*]}
}

recursion() {
	echo "DEBUG LIST[$FLOPS]: $@"
	LIST=($@)

	if [ "${LIST[*]}" = "1 1" ]; then
		printf "\nThe answer is: $FLOPS\n"
		exit 0
	fi

	FLOPS=$(expr $FLOPS + 1)
	LIST=($(reduceList "${LIST[*]}"))
	recursion ${LIST[*]}
}

recursion $@
