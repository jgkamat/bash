% surpisingly, this is not written in the best programming language ever.

function [pyramid] = speedStack(base,character)
	numOfSpaces = base - 1;
	pyramid = helper(1,character,numOfSpaces);
end

function [out] = helper(in1,in2,in3)
	% disp(in1);
	% disp(in2);
	% disp(in3);
	% disp("done");
	out = "";

	for i = 1:in3
		out = [out ' '];
	end

	for k = 1:in1
		out = [out in2 ' '];
	end

	out = out(1:end-1);
	if in3 > 0
		out = strvcat(out, helper(in1 + 1,in2,in3 - 1))
	end
end

% ShOwS ThE OuTPuT
% comment this out before submitting
stuff=speedStack(3, "a");
disp(stuff);
