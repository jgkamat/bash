#!/bin/bash

# Prints a pyramid!

CHAR="*"

if [ $# -lt 1 ]; then
	echo "You didn't specify a number"
fi

if [ $# -eq 2 ]; then
	CHAR="$2"
fi

NUM=$1
NUM_OF_SPACES=$(expr $NUM - 1)
NUM=1

printLevel() {
	NUM=$1
	NUM_OF_SPACES=$2
	LINE=""

	# echo $NUM $NUM_OF_SPACES

	for i in `eval echo {"1..$NUM_OF_SPACES"}`; do
		if [ $NUM_OF_SPACES -ne 0 ]; then
			LINE="$LINE "
		fi
	done

	for i in `eval echo {1..$NUM}`; do
		if [ $NUM -ne 0 ]; then
			LINE="$LINE$CHAR "
		fi
	done
	printf "$LINE\n"

	if [ $NUM_OF_SPACES -le 0 ]; then
		exit 0;
	fi

	NUM_OF_SPACES=$(expr $NUM_OF_SPACES - 1)
	NUM=$(expr $NUM + 1)

	printLevel $NUM $NUM_OF_SPACES
}

printLevel $NUM $NUM_OF_SPACES

