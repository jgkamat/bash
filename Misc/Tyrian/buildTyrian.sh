#/bin/bash

if [ $(id -u) -ne 0 ]; then
  echo "This script must be run as root. Exiting..." 1>&2
  exit 1
fi


zypper install mercurial libSDL_net-1_2-0 libSDL_net-devel libSDL-1_2-0 

cd /opt/

hg clone https://code.google.com/p/opentyrian/

cd opentyrian/

make release

wget -c http://sites.google.com/a/camanis.net/opentyrian/tyrian/tyrian21.zip

unzip -j tyrian21.zip -d data

rm tyrian21.zip

