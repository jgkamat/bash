#!/usr/bin/env bash

# Needs xdotool

# Wait for chrome to become focused. Remeber to go to pass through mode
echo "SET YOUR BROWSER TO INSERT MODE"
echo "Sleeping for 10 seconds..."
sleep 10
echo "Starting automation..."
NUM=0
MAX=5

while [ $NUM -lt $MAX ]; do

	xdotool key 9; xdotool key Tab
	xdotool key Down; xdotool key Tab
	xdotool key a; xdotool key Tab
	xdotool key 6; xdotool key Tab
	xdotool key Down; xdotool key Tab
	xdotool key p; xdotool key Tab
	# 'Newline'
	xdotool key Tab


	xdotool key 1; xdotool key 2;xdotool key Tab
	xdotool key Down; xdotool key Tab
	xdotool key p; xdotool key Tab
	xdotool key 1; xdotool key Tab
	xdotool key Down; xdotool key Tab
	xdotool key p

	# Go to next day
	xdotool key Tab
	xdotool key Tab
	xdotool key Tab
	xdotool key Tab
	xdotool key Tab
	xdotool key Tab
	xdotool key Tab

	echo "Finished day $NUM"
	let NUM=NUM+1
	sleep 1
done

