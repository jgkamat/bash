#!/bin/bash

# A simple bash script for managing the trello board.
#
# Requires xdotool, wmctrl and a firefox-like browser to be installed.
# These are in the debian/ubuntu repositories.

# browser to use
BROWSER=iceweasel
# This script counts on the browser displaying the same name in it's title as above.

CORE=https://trello.com/b/yuet562s/core # Core trello
ROBOCUP=https://trello.com/b/daKmddj5/robocup # RoboCup trello
IGVC=https://trello.com/b/dckfhJvL/igvc # IGVC trello
RACECAR=https://trello.com/b/3GpoDCz9/iarrc # Racecar trello
BATTLEBOTS=https://trello.com/b/BGNhxhsY/battlebots # BattleBots trello
OUTREACH=https://trello.com/b/pFyJOGfd/first # Outreach trello (Theres nothing here)

# Number of seconds to wait between cycles
BOARD_CYCLE=10


xdotool search --name $BROWSER > /dev/null
if [ $? -eq 0 -a "$1" != "-f" ]; then
	printf "ERROR: An Instance of $BROWSER is already started. \nPlease close it or use the -f flag\n\n"
	exit 1
fi

# Starts firefox with all trello tabs open
$BROWSER $CORE $ROBOCUP $IGVC $RACECAR $BATTLEBOTS $OUTREACH &

# Wait for browser to start up.
printf "Searching for $BROWSER\n"
xdotool search --name $BROWSER > /dev/null
while [ $? -ne 0 ]; do
	sleep 5
	printf "Searching for $BROWSER\n"
	xdotool search --name $BROWSER > /dev/null
done

sleep 5 # Just in case, give it a little more time to start up

# Make firefox fullscreen!
# go to firefox
wmctrl -a $BROWSER
# Press F11 to go fullscreen
xdotool key F11

# Continue running until browser crashes
while [ -n "`ps aux | grep $BROWSER | grep -v 'grep'`" ]; do
	# go to firefox
	wmctrl -a $BROWSER
	# Go to next tab
	xdotool key Ctrl+Tab

	sleep $BOARD_CYCLE;
done

