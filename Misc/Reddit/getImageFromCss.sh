#!/bin/bash
#
# This is a script to grab the images/gifs from an emotes css file.
# For an example file, donwnload: https://b.thumbs.redditmedia.com/SOu-PjHGBLI8oJgqOX2O4wZyUU5srDKSPO1wUKJrTlk.css
#
# Should work fine if you have gawk, wget, and imagemagick installed and you run this with bash.

# Exit out on a single failure
set -e
# set -x # uncomment this if you want to see every running command.

usage() {
	cat <<-EOF
	Usage: $(basename "${BASH_SOURCE}") [filename.css | http://example.com/css-file.css]

	When supplied with a file, this script will directly use it. Otherwise, it will download the css link and use that.

	This script will scrape emote formatted links from the file you provide and output them to ./out
	EOF
}

if [ "$1" == "-h" -o "$1" == "--help" ]; then
	usage
	exit 0
fi

if [ $# -ne 1 ]; then
	usage
	exit 1
fi

if ! command -v gawk >/dev/null ; then
	echo "[ERR] Please install gnu awk"
	exit 1
fi

if ! command -v wget >/dev/null ; then
	echo "[ERR] Please install wget"
	exit 1
fi

if ! command -v convert >/dev/null ; then
	echo "[ERR] Please install imagemagick"
	exit 1
fi

FILENAME="$1"
WORKING_DIR=`mktemp -d`

if [ -n "$(echo $FILENAME | grep '\.css$')" ] && ! [ -f "$FILENAME" ]; then
	echo "[INFO] Looks like you supplied a link. Downloading that css and using that..."
	# our thingy ends with css
	wget -nc -nv -P "$WORKING_DIR" "$FILENAME"
	FILENAME="$WORKING_DIR/$(echo "$FILENAME" | awk 'BEGIN {FS="/"} {print $NF}')"
fi

if ! [ -f "$FILENAME" ]; then
	echo "[ERR] $FILENAME was not a file or a link!"
	usage
	exit 1
fi

if [ -a "$(pwd)/out" ]; then
	read -p "[PROMPT] Looks like $(pwd)/out already exists. Would you like to overwrite the contents? [y/N]:" yn

	case $yn in
		[Yy]* ) ;;
		* ) echo "Bye!"; exit 0;;
	esac
fi

if [ $(wc $FILENAME | awk '{print $1}ID') -ne 0 ]; then
	# Remove newlines
	gawk -i inplace 1 ORS=' ' $FILENAME
	# Remove leading whitespace
	sed -i 's/^\s*//' $FILENAME
fi

# Done preprocessing the css file to get consistency.

# So that we get arrays for \n
IFS=$'\r\n' GLOBIGNORE='*'
RAW_DATA=($(cat $FILENAME | grep -Po 'href=.*?background-image.*?}'))

mkdir -p "$(pwd)/out/icons"
OUTPUT_DIR="$(pwd)/out"
ICON_DIR="$(pwd)/out/icons"

for i in "${RAW_DATA[@]}"; do

	# echo $i

	NAME="$(echo $i | grep -Po '(?<=href=".).*?(?="])' | head -n1)"
	WIDTH="$(echo $i | grep -Po '(?<=width:).*?(?=px)')"
	HEIGHT="$(echo $i | grep -Po '(?<=height:).*?(?=px)')"

	TMP_VAR="$(echo $i | sed -r 's/.*background-position:-?([0-9]*)px -?([0-9]*)px.*/\1 \2/')"

	X_COORD="$(echo $TMP_VAR | awk '{print $1}')"
	Y_COORD="$(echo $TMP_VAR | awk '{print $2}')"


	LINK="$(echo $i | grep -Po '(?<=background-image:url\(").*?(?="\))')"
	LINK="http:$LINK"

	IMAGE_FILENAME="$WORKING_DIR/$(echo $LINK | awk 'BEGIN {FS="/"} {print $NF}')"

	# Some matches are stupid, and aren't real. This filter should exclude them.
	if [ "$(echo $i | wc -c)" -gt 400 ]; then
		echo "[WARN] The source for image "$NAME" was too long. Check to see if it actually exists and can be converted."
		continue
	fi

	# Check for empty.
	if [ -z "$LINK" -o -z "$NAME" -o -z "$WIDTH" -o -z "$HEIGHT" -o -z "$X_COORD" -o -z "$Y_COORD" -o -z "$IMAGE_FILENAME" ]; then
		echo "[ERR] An internal error occured at: $i"
		echo "[ERR] Please file a bug report with your original input file and this message."
		printf "[DEBUG TRACE] \n\nLINK=$LINK\nNAME=$NAME\nWIDTH=$WIDTH\nHEIGHT=$HEIGHT\nX_COORD=$X_COORD\nY_COORD=$Y_COORD\nIMAGE_FILENAME=$IMAGE_FILENAME\n\n"
		exit 1
	fi

	# Put in icon dir if needed
	if [ "$WIDTH" -le 20 -a "$HEIGHT" -le 20 ]; then
		OUT_DIR=$ICON_DIR
	else
		OUT_DIR=$OUTPUT_DIR
	fi

	wget -nc -nv -P "$WORKING_DIR" "$LINK"

	# If we are dealing with an animation, take drastic masures
	if [ -n "$(echo "$i" | grep 'webkit-animation')" ]; then
		if [ -n "$(echo "$i" | grep -v 'steps')" ]; then
			echo "[WARN] Skipping $NAME as it is a webkit animation and cannot be directly made into a gif."
			continue
		fi
		# Convert seconds to ms
		i="$(echo $i| sed 's/\([0-9]\+\)s/\1000ms/g')"

		STEPS="$(echo $i | grep -Po '(?<=steps\()[0-9]*(?=\))' | head -n1)"
		LENGTH="$(echo $i | grep -Po '(?<= )[0-9]*(?=ms)' | head -n1)"

		# Lets assume that length will devide evenly. For some reason things need to be faster.
		PAUSE="$(expr "$LENGTH" / \( "$STEPS" \* 10 \))"

		ANIMATION_DIR=`mktemp -d`
		COUNTER=0

		echo "[INFO] Starting gif conversion of ${NAME}..."
		if [ "$(identify -format '%[w]\n' "$IMAGE_FILENAME")" -lt "$(expr "$X_COORD" + "$WIDTH" \* "$STEPS")" ]; then
			# vertical animation
			while [ "$COUNTER" -lt "$STEPS" ]; do
				convert -crop "${WIDTH}x${HEIGHT}+${X_COORD}+$(expr ${Y_COORD} + ${HEIGHT} \* ${COUNTER})" +repage "$IMAGE_FILENAME" "$ANIMATION_DIR/$(expr 10 + ${COUNTER}).png"
				COUNTER="$(expr $COUNTER + 1)"
			done
		else
			while [ "$COUNTER" -lt "$STEPS" ]; do
				convert -crop "${WIDTH}x${HEIGHT}+$(expr ${X_COORD} + ${WIDTH} \* ${COUNTER})+${Y_COORD}" +repage "$IMAGE_FILENAME" "$ANIMATION_DIR/$(expr 10 + ${COUNTER}).png"
				COUNTER="$(expr $COUNTER + 1)"
			done
			# horizontal animation
		fi
		convert -delay ${PAUSE} -loop 0 -dispose Previous "${ANIMATION_DIR}/*" "${OUT_DIR}/${NAME}.gif"
		echo "[INFO] Gif conversion of ${NAME} done..."
		rm -rf $ANIMATION_DIR
	else
		# Normal conversion
		convert -crop "${WIDTH}x${HEIGHT}+${X_COORD}+${Y_COORD}" +repage "$IMAGE_FILENAME" "$OUT_DIR/${NAME}.png"
		echo "[INFO] Saved ${NAME}..."
	fi

done

# Clean up temporary directory we made.
rm -rf $WORKING_DIR
