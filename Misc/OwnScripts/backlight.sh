#!/bin/bash

which xbacklight
if [ echo $? -ne 0 ]; then
    # Install xbacklight
    sudo apt-get install xbacklight
fi

# increase if argument is + and decrease if negative
if [ $1 == '+' ]; then
    xbacklight -inc 10
elif [ $1 == '-' ]; then
    xbacklight -dec 10
fi
