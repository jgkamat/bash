#!/bin/bash

# run with 'watch -d -n 5 ./$THIS_SCRIPT for best effects!

cat /var/log/apache2/access.log | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -v '128.61.105.23\|192.168.1.1\|127.0.0.1' | sort | uniq
