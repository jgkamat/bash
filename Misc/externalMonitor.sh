#!/bin/bash

if ! command -v bspc; then
	echo "Not using bspwm"
fi

MONITORS="$(bspc query -M)"

xrandr --output $(echo $MONITORS | head -n1) --auto --primary
xrandr --output $(echo $MONITORS | tail -n1)  --auto --right-of $(echo $MONITORS | head -n1) --rotate left

bspc monitor $(echo $MONITORS | head -n1) -d I II III IV V VI VII VII
bspc monitor $(echo $MONITORS | tail -n1)  -d VIII IX X

killall panel
/home/jay/.xdgconf/bspwm/panel



